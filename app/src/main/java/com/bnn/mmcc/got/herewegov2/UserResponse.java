package com.bnn.mmcc.got.herewegov2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by matteo on 24/01/2016.
 */
public class UserResponse {
        @SerializedName("flag")
        @Expose
        private boolean flag;
        @SerializedName("message")
        @Expose
        private String message;
        @SerializedName("data")
        @Expose
        private UserData data;

        /**
         *
         * @return
         * The flag
         */
        public boolean isFlag() {
            return flag;
        }

        /**
         *
         * @param flag
         * The flag
         */
        public void setFlag(boolean flag) {
            this.flag = flag;
        }

        /**
         *
         * @return
         * The message
         */
        public String getMessage() {
            return message;
        }

        /**
         *
         * @param message
         * The message
         */
        public void setMessage(String message) {
            this.message = message;
        }

        /**
         *
         * @return
         * The data
         */
        public UserData getData() {
            return data;
        }

        /**
         *
         * @param data
         * The data
         */
        public void setData(UserData data) {
            this.data = data;
        }

    }
