package com.bnn.mmcc.got.herewegov2;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by matteo on 24/01/2016.
 */
public class UserData {

    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("role_id")
    @Expose
    private String roleId;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("confirmed")
    @Expose
    private String confirmed;
    @SerializedName("confirmation_code")
    @Expose
    private Object confirmationCode;
    @SerializedName("remember_token")
    @Expose
    private Object rememberToken;
    @SerializedName("deleted_at")
    @Expose
    private Object deletedAt;
    @SerializedName("profile_image")
    @Expose
    private String profileImage;
    @SerializedName("last_status")
    @Expose
    private int lastStatus;
    @SerializedName("last_data")
    @Expose
    private Object lastData;

    /**
     *
     * @return
     * The id
     */
    public int getId() {
        return id;
    }

    /**
     *
     * @param id
     * The id
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The email
     */
    public String getEmail() {
        return email;
    }

    /**
     *
     * @param email
     * The email
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     *
     * @return
     * The password
     */
    public String getPassword() {
        return password;
    }

    /**
     *
     * @param password
     * The password
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     *
     * @return
     * The roleId
     */
    public String getRoleId() {
        return roleId;
    }

    /**
     *
     * @param roleId
     * The role_id
     */
    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    /**
     *
     * @return
     * The status
     */
    public String getStatus() {
        return status;
    }

    /**
     *
     * @param status
     * The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    /**
     *
     * @return
     * The confirmed
     */
    public String getConfirmed() {
        return confirmed;
    }

    /**
     *
     * @param confirmed
     * The confirmed
     */
    public void setConfirmed(String confirmed) {
        this.confirmed = confirmed;
    }

    /**
     *
     * @return
     * The confirmationCode
     */
    public Object getConfirmationCode() {
        return confirmationCode;
    }

    /**
     *
     * @param confirmationCode
     * The confirmation_code
     */
    public void setConfirmationCode(Object confirmationCode) {
        this.confirmationCode = confirmationCode;
    }

    /**
     *
     * @return
     * The rememberToken
     */
    public Object getRememberToken() {
        return rememberToken;
    }

    /**
     *
     * @param rememberToken
     * The remember_token
     */
    public void setRememberToken(Object rememberToken) {
        this.rememberToken = rememberToken;
    }

    /**
     *
     * @return
     * The deletedAt
     */
    public Object getDeletedAt() {
        return deletedAt;
    }

    /**
     *
     * @param deletedAt
     * The deleted_at
     */
    public void setDeletedAt(Object deletedAt) {
        this.deletedAt = deletedAt;
    }

    /**
     *
     * @return
     * The profileImage
     */
    public String getProfileImage() {
        return profileImage;
    }

    /**
     *
     * @param profileImage
     * The profile_image
     */
    public void setProfileImage(String profileImage) {
        this.profileImage = profileImage;
    }

    /**
     *
     * @return
     * The lastStatus
     */
    public int getLastStatus() {
        return lastStatus;
    }

    /**
     *
     * @param lastStatus
     * The last_status
     */
    public void setLastStatus(int lastStatus) {
        this.lastStatus = lastStatus;
    }

    /**
     *
     * @return
     * The lastData
     */
    public Object getLastData() {
        return lastData;
    }

    /**
     *
     * @param lastData
     * The last_data
     */
    public void setLastData(Object lastData) {
        this.lastData = lastData;
    }

}