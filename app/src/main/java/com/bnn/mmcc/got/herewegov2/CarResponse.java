package com.bnn.mmcc.got.herewegov2;

/**
 * Created by matteo on 10/02/2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CarResponse {

    @SerializedName("flag")
    @Expose
    private boolean flag;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private List<CarData> data = new ArrayList<CarData>();

    /**
     * @return The flag
     */
    public boolean isFlag() {
        return flag;
    }

    /**
     * @param flag The flag
     */
    public void setFlag(boolean flag) {
        this.flag = flag;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The data
     */
    public List<CarData> getData() {
        return data;
    }

    /**
     * @param data The data
     */
    public void setData(List<CarData> data) {
        this.data = data;
    }

}