package com.bnn.mmcc.got.herewegov2;

/**
 * Created by matteo on 24/01/2016.
 */
public class LoginRequest {

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String email;
    public String password;

}
