package com.bnn.mmcc.got.herewegov2;

/**
 * Created by matteo on 24/01/2016.
 */
public class LoginResponse {


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String error;
    public String token;
}
